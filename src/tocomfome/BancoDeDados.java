/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tocomfome;

import Atores.Cliente;
import Atores.Empregado;
import Pagamento.Pagamento;
import Produtos.Produto;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author bmatias
 */
public class BancoDeDados {
    ArrayList<Cliente> BancoDePedidos;
    ArrayList<Empregado> BancoDeEmpregados;
    ArrayList<Produto> BancoDeProdutos;

    public BancoDeDados() {
        this.BancoDePedidos = new ArrayList<Cliente>();
        this.BancoDeEmpregados = new ArrayList<Empregado>();
        this.BancoDeProdutos = new ArrayList<Produto>();
    }
    //public void salvarEmArquivoEmpregados() throws IOException{
    //    FileWriter file = null;
    //    file = new FileWriter("arquivo.txt");
    //    PrintWriter out = new PrintWriter(file);
    //    out.println(BancoDeEmpregados);
    //    out.close();
    //}
    //public void RetornaArquivoEmpregados() throws FileNotFoundException{
    //    FileReader file = null;
    //    Empregado empregado = new Empregado();
    //    Scanner scan = new Scanner(new File("arquivo.txt"));
    //   while(true){
    //        empregado.setLogin(scan.next()); 
    //        BancoDeEmpregados.add(empregado);
            
    //    }
    //}
    public ArrayList<Cliente> getBancoDePedidos() {
        return BancoDePedidos;
    }
    public void addPedido(Cliente cliente){
        BancoDePedidos.add(cliente);
    }
    public void removerPedido(Cliente cliente){
        BancoDePedidos.remove(cliente);
    }
    public ArrayList<Empregado> getBancoDeEmpregados() {
        return BancoDeEmpregados;
    }
    public void addEmpregado(Empregado empregado){
        BancoDeEmpregados.add(empregado);
    }
    public void removerEmpregado(Empregado empregado){
        BancoDeEmpregados.remove(empregado);
    }
    public ArrayList<Produto> getBancoDeProdutos() {
        return BancoDeProdutos;
    }
    public void addProduto(Produto produto){
        BancoDeProdutos.add(produto);
    }
    public void removerProduto(Produto produto){
        BancoDeProdutos.remove(produto);
    }
}
